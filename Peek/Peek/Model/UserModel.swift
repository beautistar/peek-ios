//
//  UserModel.swift
//  Transform
//
//  Created by Yin on 24/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class UserModel {

    var user_id: Int = 0
    var user_name = ""
    var email = ""
    var password = ""
    var phone_no = ""
    var photo_url = ""
    var invite_status = ""
    
    
    
    var token: String {
        get {
            if let token = UserDefaults.standard.value(forKey: Const.KEY_TOKEN) {
                return token as! String
            }
            return ""
        }
    }
}
