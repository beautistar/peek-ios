//
//  AppDelegate.swift
//  Peek
//
//  Created by Yin on 2018/4/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import EBBannerView
import SwiftyJSON
import PushKit

var _client: SINClient?
var _call: SINCall?
var push: SINManagedPush?

var currentVC: UIViewController?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SINClientDelegate, SINCallClientDelegate, SINManagedPushDelegate,PKPushRegistryDelegate,UNUserNotificationCenterDelegate{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        push = Sinch.managedPush(with: .production)
        
        if let push = push {
        
            push.delegate = self
            push.setDesiredPushTypeAutomatically()
            push.registerUserNotificationSettings()
     
            let onUserDidLogin: ((String) -> Void) = { userId in
                push.registerUserNotificationSettings()
                self.initSinClient(user_id: userId)
            }
            
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UserDidLoginNotification"), object: nil, queue: nil) { (note) in
             
                print(note.userInfo![Const.KEY_USERID] ?? "")
                guard let userId = note.userInfo![Const.KEY_USERID] as? String else { return }
                UserDefaults.standard.set(userId, forKey: Const.KEY_USERID)
                UserDefaults.standard.synchronize()
                onUserDidLogin(userId)
            }
            
        }
        
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "UserDidLoginNotification"), object: nil, userInfo: ["userId" : currentUser?.user_id ?? ""]))
        push?.registerUserNotificationSettings()
        return true
    }
    
    func handleNotification(notification: UILocalNotification?) {
        
        if notification != nil {
            
            let result: SINNotificationResult = (_client!.relay(notification))!
            
            if result.isCall() && result.call().isTimedOut {
                let alert: UIAlertController = UIAlertController(title: "Missed Call", message: String(format: "Missed Call from %@", arguments: [result.call().remoteUserId]), preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: {_ -> Void in
                    alert.dismiss(animated: true, completion: nil)
                } )
                
                alert.addAction(okAction)
                
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var deviceTokenString = ""
        
        for i in 0..<deviceToken.count {
            deviceTokenString = deviceTokenString + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        push?.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
        
        print("deviceTokenString: \( deviceTokenString)")
        
    }
   
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Remote notification");
        
        push?.application(application, didReceiveRemoteNotification: userInfo)
    }
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        _client?.registerPushNotificationData(pushCredentials.token)
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //MARK: - clientapi.sinch.com    
    
    func initSinClient(user_id: String) {
        
        if (_client == nil) {
        //environmentHost: "clientapi.sinch.com", "sandbox.sinch.com"
            _client = Sinch.client(withApplicationKey: "961bb1ef-1d98-470d-842b-65cadd7ff33d",
                                   applicationSecret: "Mqz/y9JmXkOgaAwxHayDCg==",
                                   environmentHost: "clientapi.sinch.com",
                                   userId: user_id)
            
            guard let client = _client else { return }
            client.delegate = self
            client.call().delegate = self
            client.setSupportCalling(true)
            client.setSupportPushNotifications(true)
            client.enableManagedPushNotifications()
            client.start()
            client.startListeningOnActiveConnection()
            
        }
    }
    
    func handleRemoteNotification(_ userInfo: Any) {
        
        print("Handle Remote Notification")
        
        if (_client == nil) {
            let userId = UserDefaults.standard.string(forKey: Const.KEY_USERID)
            
            if (userId != nil) {
                self.initSinClient(user_id: userId!)
            }
        }
        
        _client?.relayRemotePushNotification(userInfo as! [AnyHashable : Any])
        
    }
    
    //MARK: - SINManagedPushDelegate
    
    func managedPush(_ managedPush: SINManagedPush!, didReceiveIncomingPushWithPayload payload: [AnyHashable : Any]!, forType pushType: String!) {
        print("Incoming push")
        let notification = SINLocalNotification()
        notification.alertAction = "Action"
        notification.alertBody = "Incoming call"
       
    }
    
    
    
    //MARK: - SINCallClientDelegate
    
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {

        print("Incoming call")
        
        let storyborad = UIStoryboard.init(name: "Main", bundle: nil)
        let callvc = storyborad.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
        callvc.call = call
        callvc.call?.delegate = callvc
        //top.present(callvc, animated: true, completion: nil)
        self.window?.rootViewController?.present(callvc, animated: true, completion: nil)
    }

    func client(_ client: SINCallClient!, localNotificationForIncomingCall call: SINCall!) -> SINLocalNotification! {
        
        print("Local Notification")
        
        let notification = SINLocalNotification()
        notification.alertAction = "Action"
        notification.soundName = "ring.wav"
        notification.alertBody = "Incoming call"
        //notification.alertBody = "Incoming call from " + call.remoteUserId
        _call = call

        return notification
    }
    
    func voipRegistration() {
      let pushpk = PKPushRegistry.init(queue: DispatchQueue.main)
        pushpk.delegate = self
        pushpk.desiredPushTypes = Set([.voIP])

    }
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        //notify
        print("pushregistry")
        let dic = payload.dictionaryPayload
        print("dic-->", dic)
        let sinchinfo = dic["sin"] as? String
        print("sinchinfo-->", sinchinfo ?? "")

        if sinchinfo == nil {
            return
        }
        DispatchQueue.main.async(execute: {
           _client?.relayRemotePushNotificationPayload(sinchinfo)
        })
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("notificationcenter")
        let state: UIApplicationState = UIApplication.shared.applicationState // or use  let state =  UIApplication.sharedApplication().applicationState
        
        if state == .background {
            
            let top: UIViewController = (self.window?.rootViewController)!
            let storyborad = UIStoryboard.init(name: "Main", bundle: nil)
            let callvc = storyborad.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            callvc.call = _call
            callvc.call?.delegate = callvc
            top.present(callvc, animated: true, completion: nil)
            
        }
    }
    //MARK: - SINClientDelegate
    
    func clientDidStart(_ client: SINClient!) {
        
        print("Sich client started successfully (version: )" + Sinch.version())
        self.voipRegistration()
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        
        print("Sinch client error : " + error.localizedDescription)
    }
    
    func client(_ client: SINClient!, logMessage message: String!, area: String!, severity: SINLogSeverity, timestamp: Date!) {
        if severity == .critical {
            print(message)
        }
    }
}
