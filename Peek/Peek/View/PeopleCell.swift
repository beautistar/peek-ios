//
//  PeopleCell.swift
//  Peek
//
//  Created by Yin on 2018/4/30.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class PeopleCell: UITableViewCell {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var acceptButtonView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var btnUnBlock: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
