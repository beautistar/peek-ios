//
//  FavoriteCell.swift
//  Peek
//
//  Created by Yin on 2018/4/30.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class FavoriteCell: UICollectionViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imvHeart: UIImageView!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnHeart: UIButton!
}
