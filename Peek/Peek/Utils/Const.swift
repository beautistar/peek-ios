//
//  Const.swift
//  Peek
//
//  Created by Yin on 2018/4/30.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

var queueVCOpened: Bool = false
var isCalled: Bool = false

class Const {
    
    static let APP_NAME                     = "Peek"
    static let OK                           = "OK"
    static let CANCEL                       = "Cancel"
    static let NO                           = "No"
    static let YES                          = "Yes"
    
    static let SAVE_ROOT_PATH               = "Transform"
    
    //error messages
    
    static let CHECK_USERNAME_EMPTY         = "Please input your username"
    static let CHECK_PASSWORD_EMPTY         = "Please input your password"
    static let CHECK_PHONE_NO_EMPTY         = "Please input your phone number"
    static let CHECK_EMAIL_EMPTY            = "Please input your email address"
    static let CHECK_EMAIL_INVALID          = "Please input valid email"
    static let CHECK_PHOTO                  = "Please attach your profile photo"
    
    static let ERROR_CONNECT                = "Failed to server connection"
    static let EXIST_EMAIL                  = "User is already registered"
    static let UNREGISTERED_USER            = "User does not registered"
    static let INVALID_LOGIN                = "Invalid Email or Password!"
    static let SUCCESS_RESETPWD             = "Password reset successfully"
    static let NO_DATA_FOUND                = "No data found"
    
    static let IS_INVITED                   = "Invited"
    
    // Params
    
    static let PARAM_USERMODEL              = "user_model"
    static let PARAM_USERID                 = "user_id"
    static let PARAM_USERNAME               = "user_name"
    static let PARAM_EMAIL                  = "email"
    static let PARAM_PASSWORD               = "password"    
    static let PARAM_PHONENO                = "phone_number"
    static let PARAM_STATUS                 = "status"
    static let PARAM_PHOTOURL               = "photo_url"
    static let PARAM_TOKEN                  = "token"
    static let PARAM_CONTACTLIST            = "contact_list"
    static let PARAM_USERLIST               = "user_list"
    static let PARAM_FAVORITELIST           = "favorite_list"
    static let PARAM_TARGET_ID              = "target_id"
    static let PARAM_BLOCKLIST              = "block_list"
    static let PARAM_TARGETID               = "target_id"
    static let PARAM_ROOMID                 = "room_id"
    
    static let FROM_GALLERY                 = "Gallery"
    static let FROM_CAMERA                  = "Camera"
    
    static let FROM_MAIN                    = 111
    static let FROM_BLACKLIST               = 222
   
    static let IS_CALLING                   = 333
    static let IS_RECEIVING                 = 444
    
    // Key
    static let KEY_USERID                   = "k_userid"
    static let KEY_TOKEN                    = "k_token"
    static let KEY_EMAIL                    = "k_email"
    static let KEY_PASSWORD                 = "k_password"
    
    // Result code
    static let RESULT_CODE                  = "result_code"
    static let RESULT_SUCCESS               = "result_success"
    static let CODE_FAIL                    = -100
    static let CODE_0                       = 0
    static let CODE_SUCCESS                 = 200
    
    static let CODE_ALREADY_REGISTERED      = 201
    static let CODE_INVALIDE_EMAIL_PWD      = 202
    static let CODE_UNREGISTERED_USER       = 203
    static let CODE_NO_DATA                 = 204
    static let CODE_205_ALREADY             = 205
}
