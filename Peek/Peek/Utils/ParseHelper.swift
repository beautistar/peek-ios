//
//  ParseHelper.swift
//  Transform
//
//  Created by Yin on 26/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation
import SwiftyJSON

class ParseHelper {
    
    static func parseLoginUser(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        let userObject = json[Const.PARAM_USERMODEL]
        user.user_id = userObject[Const.PARAM_USERID].intValue
        user.user_name = userObject[Const.PARAM_USERNAME].stringValue
        user.email = userObject[Const.PARAM_EMAIL].stringValue
        user.password = userObject[Const.PARAM_PASSWORD].stringValue
        user.photo_url = userObject[Const.PARAM_PHOTOURL].stringValue
        user.phone_no = userObject[Const.PARAM_PHONENO].stringValue
        
        return user
    }
    
    static func parseContactUser(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        
        user.user_id = json[Const.PARAM_USERID].intValue
        user.user_name = json[Const.PARAM_USERNAME].stringValue
        user.email = json[Const.PARAM_EMAIL].stringValue
        user.photo_url = json[Const.PARAM_PHOTOURL].stringValue
        user.phone_no = json[Const.PARAM_PHONENO].stringValue
        user.invite_status = json[Const.PARAM_STATUS].stringValue
        return user
    }
    
    static func parseBlockUser(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        
        user.user_id = json[Const.PARAM_USERID].intValue
        user.user_name = json[Const.PARAM_USERNAME].stringValue
        user.email = json[Const.PARAM_EMAIL].stringValue
        user.photo_url = json[Const.PARAM_PHOTOURL].stringValue
        user.phone_no = json[Const.PARAM_PHONENO].stringValue
        
        return user
    }
    
    
}
