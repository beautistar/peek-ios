//
//  ApiRequest.swift
//  Transform
//
//  Created by Yin on 24/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiRequest {
    
    static let BASE_URL                     = "http://13.126.68.181/"
    static let SERVER_URL                   = BASE_URL + "app/api/"
    
    static let REQ_SIGNUP                   = SERVER_URL + "signup"
    static let REQ_SIGNIN                   = SERVER_URL + "signin"
    static let REQ_FORGOTPASSWORD           = SERVER_URL + "forgotPassword"
    static let REQ_RESETPASSWORD            = SERVER_URL + "resetPassword"
    static let REQ_SEARCHBYNAME             = SERVER_URL + "searchByName"
    static let REQ_ADDFAVORITE              = SERVER_URL + "addFavorite"
    static let REQ_SENDREQUEST              = SERVER_URL + "sendRequest"
    static let REQ_ACCEPTREQUEST            = SERVER_URL + "acceptRequest"
    static let REQ_SENDINVITE               = SERVER_URL + "sendInvite"
    static let REQ_ACCEPTINVITE             = SERVER_URL + "acceptInvite"
    static let REQ_GETCONTACTLIST           = SERVER_URL + "getContactList"
    static let REQ_GETFAVORITELIST          = SERVER_URL + "getFavoriteList"
    static let REQ_REGISTER_TOKEN           = SERVER_URL + "registerToken"
    static let REQ_UPLOAD_PHOTO             = SERVER_URL + "uploadPhoto"
    static let REQ_BLOCKUSER                = SERVER_URL + "blockUser"
    static let REQ_GETBLOCKLIST             = SERVER_URL + "getBlockList"
    static let REQ_UNBLOCKUSER              = SERVER_URL + "unBlockUser"
    static let REQ_REMOVEFAVORITE           = SERVER_URL + "removeFavorite"
    static let REQ_GETUSERINFO              = SERVER_URL + "getUserById"
        
    static func signup(_ user: UserModel, completion: @escaping (Int, Int) -> ()) {
        
        let params = [Const.PARAM_USERNAME: user.user_name,
                      Const.PARAM_EMAIL: user.email,
                      Const.PARAM_PHONENO: user.phone_no,
                      Const.PARAM_PASSWORD: user.password] as [String : Any]
        
        Alamofire.request(REQ_SIGNUP, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                
                let dict = JSON(response.result.value!)
                let resultCode = dict[Const.RESULT_CODE].intValue
                if resultCode == Const.CODE_SUCCESS {
                     let userId = dict[Const.PARAM_USERID].intValue
                    
                    UserDefaults.standard.set(user.email, forKey: Const.KEY_EMAIL)
                    UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
                    
                    completion(resultCode, userId)
                } else if resultCode == Const.CODE_ALREADY_REGISTERED {
                    completion(resultCode, Const.CODE_0)
                }
                break
            case .failure(let error):
                completion(Const.CODE_FAIL, Const.CODE_0)
                print(error)
            }
        }
    }
    
    static func uploadPhoto(_ imgURL: String, userId: Int, completion: @escaping (Int,  String) -> ()) {
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath: imgURL), withName: "file")
                multipartFormData.append(String(userId).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_USERID)
        },
            to: REQ_UPLOAD_PHOTO,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        switch response.result {
                            
                        case .success(_):
                            let json = JSON(response.result.value!)
                            let resCode = json[Const.RESULT_CODE].intValue
                            if (resCode == Const.CODE_SUCCESS) {
                                let imageurl = json[Const.PARAM_PHOTOURL].stringValue
                                completion(Const.CODE_SUCCESS, imageurl)
                            }
                            else {
                                completion(Const.CODE_FAIL, "")
                            }
                        case .failure(_):
                            
                            completion(Const.CODE_FAIL, "")
                        }
                    }
                    
                case .failure(_):
                    completion(Const.CODE_FAIL, "")
                }
        })
    }
    
    static func signin(email: String, password: String, completion: @escaping (String) -> () ) {
        
        Alamofire.request(REQ_SIGNIN, method: .post, parameters: [Const.PARAM_EMAIL : email, Const.PARAM_PASSWORD : password]).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    currentUser = ParseHelper.parseLoginUser(json)
                    
                    UserDefaults.standard.set(email, forKey: Const.KEY_EMAIL)
                    UserDefaults.standard.set(password, forKey: Const.KEY_PASSWORD)
                    
                    completion(Const.RESULT_SUCCESS)
                    
                }
                else if resCode == Const.CODE_INVALIDE_EMAIL_PWD {
                    completion(Const.INVALID_LOGIN)
                }
                
            }
        }
    }
    
    static func forgotPassword(email: String, completion: @escaping (String) -> () ) {
        
        Alamofire.request(REQ_FORGOTPASSWORD, method: .post, parameters: [Const.PARAM_EMAIL : email]).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    completion(Const.RESULT_SUCCESS)
                }
                    
                else if resCode == Const.CODE_UNREGISTERED_USER {
                    completion(Const.UNREGISTERED_USER)
                }
            }
        }
    }
    
    static func resetPassword(email: String, password: String, completion: @escaping (String) -> () ) {
        
        Alamofire.request(REQ_RESETPASSWORD, method: .post, parameters: [Const.PARAM_EMAIL : email, Const.PARAM_PASSWORD : password]).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {                   
                   
                    UserDefaults.standard.set(password, forKey: Const.KEY_PASSWORD)
                    
                    completion(Const.RESULT_SUCCESS)
                    
                }
                else if resCode == Const.CODE_INVALIDE_EMAIL_PWD {
                    completion(Const.INVALID_LOGIN)
                }
                
            }
        }
    }
    
    static func getContactList(userId: Int, completion: @escaping(Int, [UserModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: userId] as [String : Any]
        
        Alamofire.request(REQ_GETCONTACTLIST, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var contacts = [UserModel]()
                    for contactObject in json[Const.PARAM_CONTACTLIST].arrayValue {
                        contacts.append(ParseHelper.parseContactUser(contactObject))
                    }
                    completion(Const.CODE_SUCCESS, contacts)
                } else if resCode == Const.CODE_NO_DATA {
                    completion(Const.CODE_NO_DATA, [])
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }

    
    static func getSearchList(searchString: String, completion: @escaping(Int, [UserModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_USERNAME: searchString] as [String : Any]
        
        Alamofire.request(REQ_SEARCHBYNAME, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var contacts = [UserModel]()
                    for contactObject in json[Const.PARAM_USERLIST].arrayValue {
                        contacts.append(ParseHelper.parseContactUser(contactObject))
                    }
                    completion(Const.CODE_SUCCESS, contacts)
                } else if resCode == Const.CODE_NO_DATA {
                    completion(Const.CODE_NO_DATA, [])
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func sendInvite(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_SENDINVITE, method: .post, parameters: params).responseJSON { response in
            
            print("sendInvite-->", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func acceptInvite(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_ACCEPTINVITE, method: .post, parameters: params).responseJSON { response in
            
            print("acceptInvite-->", response)
            
            if response.result.isSuccess {                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func addFavorite(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_ADDFAVORITE, method: .post, parameters: params).responseJSON { response in
            
            print("addFavorite-->", response)
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getFavoriteList(completion: @escaping(Int, [UserModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id] as [String : Any]
        
        Alamofire.request(REQ_GETFAVORITELIST, method: .post, parameters: params).responseJSON { response in
            
            print("getFavorite", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var contacts = [UserModel]()
                    for contactObject in json[Const.PARAM_FAVORITELIST].arrayValue {
                        contacts.append(ParseHelper.parseContactUser(contactObject))
                    }
                    completion(Const.CODE_SUCCESS, contacts)
                } else if resCode == Const.CODE_NO_DATA {
                    completion(Const.CODE_NO_DATA, [])
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func blockUser(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_BLOCKUSER, method: .post, parameters: params).responseJSON { response in
            
            print("blockUser-->", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getFBlockList(completion: @escaping(Int, [UserModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id] as [String : Any]
        
        Alamofire.request(REQ_GETBLOCKLIST, method: .post, parameters: params).responseJSON { response in
            
            print("getBlockList", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var contacts = [UserModel]()
                    for contactObject in json[Const.PARAM_BLOCKLIST].arrayValue {
                        contacts.append(ParseHelper.parseBlockUser(contactObject))
                    }
                    completion(Const.CODE_SUCCESS, contacts)
                } else if resCode == Const.CODE_NO_DATA {
                    completion(Const.CODE_NO_DATA, [])
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func unBlockUser(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_UNBLOCKUSER, method: .post, parameters: params).responseJSON { response in
            
            print("unBlockUser-->", response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func removeFavorite(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_REMOVEFAVORITE, method: .post, parameters: params).responseJSON { response in
            
            print("removeFavorite-->", response)
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func saveToken(token: String, completion: @escaping(Int) -> ()) {
        if let user = currentUser {
            let params = [Const.PARAM_USERID: user.user_id,
                          Const.PARAM_TOKEN: token] as [String: Any]
            
            Alamofire.request(REQ_REGISTER_TOKEN, method: .post, parameters: params).responseJSON { response in
                
                print(response)
                
                if response.result.isSuccess {
                    completion(Const.CODE_SUCCESS)
                } else {
                    completion(Const.CODE_FAIL)
                }
            }
        }
    }
    
    static func sendRequest(target_id: Int, room_id: String, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_ROOMID: room_id,
                      Const.PARAM_TARGETID: target_id] as [String: Any]
        
        Alamofire.request(REQ_SENDREQUEST, method: .post, parameters: params).responseJSON { response in
            
            print("send_request->", response)
            
            if response.result.isSuccess {
                
                let dict = JSON(response.result.value!)
                
                let resultCode = dict[Const.RESULT_CODE].intValue
                    completion(resultCode)
            }
        }
    }
    
    
    static func acceptRequest(target_id: Int, room_id: String, completion: @escaping(Int, Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_ROOMID: room_id,
                      Const.PARAM_TARGETID: target_id] as [String: Any]
        
        Alamofire.request(REQ_ACCEPTREQUEST, method: .post, parameters: params).responseJSON { response in
            
            print("accept requet->" , response)
            
            if response.result.isSuccess {
                
                let dict = JSON(response.result.value!)
                
                let resultCode = dict[Const.RESULT_CODE].intValue
                if resultCode == Const.CODE_SUCCESS {
                    let room_no = dict[Const.PARAM_ROOMID].intValue
                    completion(Const.CODE_SUCCESS, room_no)
                    
                } else {
                    completion(Const.CODE_FAIL, 0)
                }
            }
        }
    }
    
    static func sendRequest(target_id: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.user_id,
                      Const.PARAM_TARGET_ID: target_id] as [String : Any]
        
        Alamofire.request(REQ_SENDREQUEST, method: .post, parameters: params).responseJSON { response in
            
            print("sendRequest-->", response)
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getUserInfo(user_id: Int, completion: @escaping(Int, UserModel) -> ()) {
        
        let params = [Const.PARAM_USERID: user_id] as [String: Any]
        
        Alamofire.request(REQ_GETUSERINFO, method: .post, parameters: params).responseJSON { response in
            
            print("getUserById", response)
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    let user: UserModel = ParseHelper.parseLoginUser(json)
                    completion(Const.CODE_SUCCESS, user)
                }
            } else {
                completion(Const.CODE_FAIL, UserModel())
            }
        }
    }
}
















