//
//  BlockListViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/29.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class BlockListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblBlockList: UITableView!
    weak var delegate: LeftMenuProtocol?

    var btnSearch : UIBarButtonItem!
    
    var blockList = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setNavigationBarItem()
        
        tblBlockList.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.removeNavigationBarItem()
        
        btnSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_search"), style:.plain,target: self, action: #selector(searchAction))
        btnSearch?.image = btnSearch?.image?.withRenderingMode(.alwaysOriginal)
        
        self.navigationItem.rightBarButtonItem  = btnSearch
        
        getBlockList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: SettingsViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
   
    @objc func searchAction() {
        
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC._from = Const.FROM_BLACKLIST
        self.navigationController?.pushViewController(searchVC, animated: true)
        
    }
    
    @objc func unBlockAction(_ sender: UIButton) {
        
        let target_id = blockList[sender.tag].user_id
        
        self.showLoadingView()
        ApiRequest.unBlockUser(target_id: target_id) { (result_code) in
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.showToast("success")
            } else if result_code == Const.CODE_205_ALREADY {
                self.showToast("Already unBlocked")
            }
            self.blockList.remove(at: sender.tag)
            self.tblBlockList.reloadData()
        }
        
    }
    
    func getBlockList() {
        
        self.showLoadingView()
        
        ApiRequest.getFBlockList { (rescode, blockUsers) in
            
            self.hideLoadingView()
            
            if rescode == Const.CODE_SUCCESS {
                
                self.blockList = blockUsers
                self.tblBlockList.reloadData()
            }
        }
    }

    // MARK:- tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleCell") as! PeopleCell
        let blockUser = self.blockList[indexPath.row]
        cell.btnUnBlock.tag = indexPath.row
        cell.btnUnBlock.addTarget(self, action: #selector(unBlockAction(_:)), for: .touchUpInside)
        cell.lblName.text = blockUser.user_name
        cell.imvPhoto.sd_setImage(with: URL(string: blockUser.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblStatus.isHidden = true
        return cell
    }
}
