//
//  signupViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/29.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var imvProfile: UIImageView!
    
    var user = UserModel()
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgUrl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        imvProfile.layer.borderColor = UIColor.lightGray.cgColor
        imvProfile.layer.borderWidth = 2
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closeTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func attachTapped(_ sender: Any) {
        
        // for test
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            && UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let galleryAction: UIAlertAction = UIAlertAction(title: Const.FROM_GALLERY, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let cameraAction: UIAlertAction = UIAlertAction.init(title: Const.FROM_CAMERA, style: UIAlertActionStyle.default, handler: { (cameraSourceAlert) in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(galleryAction)
            photoSourceAlert.addAction(cameraAction)
            photoSourceAlert.addAction(UIAlertAction(title: Const.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.imvProfile.image = pickedImage
            _imgUrl = saveToFile(image: pickedImage, filePath: Const.SAVE_ROOT_PATH, fileName: "profile.png")
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func isValid() -> Bool {
        
        if tfUserName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_USERNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPhoneNumber.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHONE_NO_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if _imgUrl.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PHOTO, positive: Const.OK, negative: nil)
            return false
            
        }
        
        return true
    }

    @IBAction func signupTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if isValid() {
            
            /// do register
            
            user.email = tfEmail.text!
            user.user_name = tfUserName.text!
            user.password = tfPassword.text!
            user.phone_no = tfPhoneNumber.text!
            
            self.showLoadingView()
            ApiRequest.signup(user, completion: { (resultCode, user_id)  in
                
                if resultCode == Const.CODE_SUCCESS {
                    self.user.user_id = user_id
                    currentUser = self.user
                    self.uploadPhoto()
                } else if resultCode == Const.CODE_ALREADY_REGISTERED {
                    self.hideLoadingView()
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.EXIST_EMAIL, positive: Const.OK, negative: nil)
                } else {
                    self.hideLoadingView()
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                }
            })
        }
    }
    
    func uploadPhoto() {
        
        ApiRequest.uploadPhoto(_imgUrl, userId: (currentUser?.user_id)!, completion: { (resultCode, photoUrl) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                currentUser?.photo_url = photoUrl
                ApiRequest.saveToken(token: (currentUser?.token)!, completion: { (rescode) in })
                
                self.gotoMain()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        })
    }
    
    func gotoMain() {
        
        createMenuView()
        
    }
    
    
    fileprivate func createMenuView() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "ffffff")//B3E5FB
        nvc.navigationBar.barTintColor = UIColor(hex: "9EE5FB")
        
        menuViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }

}
