//
//  ForgotPwdViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/29.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class ForgotPwdViewController: BaseViewController {

    
    @IBOutlet weak var tfEmail: UITextField!
    
    var password: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func closeTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func submitTapped(_ sender: Any) {
        
        tfEmail.resignFirstResponder()
        
        if isValid() {
            self.forgotPassword()
        }
    }
    
    func isValid() -> Bool {
        
        if tfEmail.text?.count == 0 {
            
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func forgotPassword() {
        
        self.showLoadingView()
        
        ApiRequest.forgotPassword(email: tfEmail.text!, completion: { (message)  in
            
            self.hideLoadingView()
            if message == Const.RESULT_SUCCESS {
                
                // reset Password
                
                self.resetPassword()
                
            } else {
                
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
                
            }
        })
        
    }
    
    func resetPassword() {
        
        var mytextField : UITextField?
        
        let alert = UIAlertController(title: Const.APP_NAME, message: "Please input new password", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "New Password"
            textField.isSecureTextEntry = true
            mytextField = textField
        }
        alert.addAction(UIAlertAction(title: Const.CANCEL, style: .default, handler: {(_) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Submit", style: .default, handler: { (_) in
            //let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            if let text = mytextField?.text {
                if text.count != 0 {
                    self.password = text
                    self.resetPasswordProcess()
                }
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func resetPasswordProcess() {
        
        self.showLoadingView()
        
        ApiRequest.resetPassword(email: tfEmail.text!, password: password, completion: { (message)  in
            self.hideLoadingView()
            if message == Const.RESULT_SUCCESS {
                self.showToast(Const.SUCCESS_RESETPWD)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        })
    }
}
