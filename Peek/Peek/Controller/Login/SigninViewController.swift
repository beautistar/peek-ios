//
//  SigninViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/29.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class SigninViewController: BaseViewController {

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    
    var appDelegate: AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        if let email = UserDefaults.standard.string(forKey: Const.KEY_EMAIL) {
            if let password = UserDefaults.standard.string(forKey: Const.KEY_PASSWORD) {
                
                tfEmail.text = email
                tfPassword.text = password
                self.signinTapped((Any).self)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func isValid() -> Bool {
        
        if tfEmail.text?.count == 0 {
            
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }

    @IBAction func signinTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if isValid() {
            self.doLogin()
        }
    }
    
    func doLogin() {
        
        let email = tfEmail.text!
        let password = tfPassword.text!
        
        self.showLoadingView()
        
        ApiRequest.signin(email: email, password: password, completion: { (message)  in
            
            self.hideLoadingView()
            if message == Const.RESULT_SUCCESS {                
                
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "UserDidLoginNotification"), object: nil, userInfo: ["userId" : currentUser?.user_id ?? ""]))
                
                if _client != nil {
                    
                    if !(_client?.isStarted())! {
                        _client?.start()
                        _client?.startListeningOnActiveConnection()
                    }
                } else {
                    self.appDelegate?.initSinClient(user_id: "\(currentUser!.user_id)")
                }

                self.createMenuView()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        })
    }
    
    fileprivate func createMenuView() {
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "ffffff")
        nvc.navigationBar.barTintColor = UIColor(hex: "9EE5FB")
        
        menuViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController        
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }

}
