//
//  MenuViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/29.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

import SDWebImage

enum LeftMenu: Int {
    case main = 0
    case notification
    case blocklist
    case settings
    case about
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController, LeftMenuProtocol {

    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tblMenu: UITableView!
    
    var menusNames = ["Contact", "Notification", "Block List", "Settings", "About"]
    
    var menuIcons = [#imageLiteral(resourceName: "contact"), #imageLiteral(resourceName: "notification"), #imageLiteral(resourceName: "block"), #imageLiteral(resourceName: "settings"), #imageLiteral(resourceName: "heart")]
    
    var mainViewController: UIViewController!
    var notificationViewController: UIViewController!
    var blockListViewController: UIViewController!
    var settingsViewController: UIViewController!
    var aboutViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
        
        imvProfile.layer.borderColor = UIColor.gray.cgColor
        imvProfile.layer.borderWidth = 2
        
        imvProfile.sd_setImage(with: URL(string: (currentUser?.photo_url)!), placeholderImage: #imageLiteral(resourceName: "img_user"))
        lblName.text = currentUser?.user_name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tblMenu.tableFooterView = UIView()
        
        let mainTabViewController = storyboard?.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)
        
        let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.notificationViewController = UINavigationController(rootViewController: notificationVC)
        
        let blockListVC = self.storyboard?.instantiateViewController(withIdentifier: "BlockListViewController") as! BlockListViewController
        self.blockListViewController = UINavigationController(rootViewController: blockListVC)
        
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        settingsVC.delegate = self
        self.settingsViewController = UINavigationController(rootViewController: settingsVC)
        
        let aboutVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        aboutVC.delegate = self
        self.aboutViewController = UINavigationController(rootViewController: aboutVC)
        
    }

    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .notification:
            self.slideMenuController()?.changeMainViewController(self.notificationViewController, close: true)
        case .blocklist:
            self.slideMenuController()?.changeMainViewController(self.blockListViewController, close: true)
        case .settings:
            self.slideMenuController()?.changeMainViewController(self.settingsViewController, close: true)
        case .about:
            self.slideMenuController()?.changeMainViewController(self.aboutViewController, close: true)
        }
    }
}

extension MenuViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .notification, .blocklist, .settings, .about:
                return 50
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblMenu == scrollView {
            
        }
    }
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menusNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .notification, .blocklist, .settings, .about:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
                cell.imvIcon.image = menuIcons[indexPath.row]
                cell.lblName.text = menusNames[indexPath.row]
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
}

