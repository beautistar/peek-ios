//
//  SinCallViewController.swift
//  Peek
//
//  Created by Yin on 2018/5/26.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import EBBannerView

class SinCallViewController: BaseViewController, SINCallClientDelegate, SINCallDelegate, AVAudioPlayerDelegate {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var statusView: UIView!
    
    var targetUser = UserModel()
    
//    var _client: SINClient?
//    var _call: SINCall?

    var ringTimer: Timer!
    var connectingTimer: Timer!
    var durationTimer: Timer!
    
    var player : AVAudioPlayer?
    var cancelStatus = false
    
    var loaded = false
    
    var statusVC = CallStatusViewController()
    
    var roomStatus = Const.IS_CALLING
    
    var audioController : SINAudioController {
        return (_client?.audioController())!
    }
    
    func setCall(_ call: SINCall) {
        _call = call
        _call?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //initSinClient()
        
        //(UIApplication.shared.delegate as? AppDelegate)?.videoCallStautsDelegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imvPhoto.layer.borderColor = UIColor.gray.cgColor
        imvPhoto.layer.cornerRadius = 50
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.masksToBounds = true
        lblStatus.text = "Connecting..."
        
        imvPhoto.sd_setImage(with: URL(string: targetUser.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        lblName.text = targetUser.user_name
        
        
        if connectingTimer != nil && connectingTimer.isValid {
            connectingTimer.invalidate()
        }
        
        if _call?.direction == SINCallDirection.incoming {
            //playAudio()
        } else {
            lblStatus.text = "Calling..."
        }
        
        if roomStatus == Const.IS_CALLING {
            connectingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(connectCall), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        disconnect()
        pauseAudio()
        
    }
    
    @objc func connectCall() {
        if (_client?.isStarted())! {
            makeCall()
            
            
            ApiRequest.sendRequest(target_id: targetUser.user_id) { (resCode) in
                
            }
            
            connectingTimer.invalidate()
        }
    }
    
    @objc func makeCall() {
        
        if _call == nil {            
            _call = _client?.call().callUser(withId: "\(targetUser.user_id)")
            _call?.delegate = self
            playAudio()
            audioController.enableSpeaker()
        } else {
            _call?.hangup()
            _call = nil
        }
    }
    
    func disconnect() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        
        _call?.hangup()
        self.dismiss(animated: true, completion: nil)
    }
    
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        
        let banner = EBBannerView.banner({ (make) in
            make?.style = EBBannerViewStyle(rawValue: 11)
            make?.icon = UIImage.init(named: "AppIcon")
            make?.title = Const.APP_NAME
            make?.content = "Peek call"
            make?.date = "Now"
        })
        banner?.show()
        
        call.delegate = self
        call.answer()
        _call = call
        lblStatus.text = "Connected"
        audioController.enableSpeaker()
    }
    
    @objc func onDurationTimer(_ unused: Timer) {
        let duration: Int = Int(Date().timeIntervalSince((_call?.details.establishedTime)!))
        lblStatus.text = String.init(format: "%02d:%02d", Int(duration / 60), Int(duration % 60))
    }
    
    // MARK: - Sinch Call Delegate
    
    func callDidProgress(_ call: SINCall!) {
        lblStatus.text = "Ringing..."
        print("callDidProgress")
        
        playSinAudio()
    }
    
    func callDidEstablish(_ call: SINCall!) {
        
        let selectorAsString = NSStringFromSelector(#selector(onDurationTimer(_:)))
        durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(internal_updateDuration), userInfo: selectorAsString, repeats: true)

        //lblStatus.text = "Connected"
        print("calDidEstablish")
        pauseAudio()
        
    }
    
    @objc func internal_updateDuration(_ timer: Timer) {
        let selector = NSSelectorFromString(timer.userInfo as! String)
        if self.responds(to: selector) {
            self.perform(selector, with: timer)
        }
    }
    
    func callDidEnd(_ call: SINCall!) {
        print("callDidEnd")
        //self.callButton.setTitle("Call", for: .normal)
        _call = nil
        self.dismiss(animated: true, completion: nil)
        self.durationTimer.invalidate()
        self.durationTimer = nil
    }
    
    func pauseAudio() {
        if ringTimer != nil && ringTimer.isValid {
            ringTimer.invalidate()
        }
        if player != nil {
            player?.pause()
        }
    }
    
    func playSinAudio() {
        guard let url = Bundle.main.path(forResource: CommonUtils.getCallStatusAudioFileName(roomStatus), ofType: nil)  else {
            return
        }
        
        let audioplayer = _client?.audioController()
        audioplayer?.startPlayingSoundFile(url, loop: true)
    }
    
    @objc func playAudio(){
        guard let url = Bundle.main.url(forResource: CommonUtils.getCallStatusAudioFileName(roomStatus), withExtension: nil)  else {
            return
        }
        
        do {
            player = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            guard let player = player else { return }
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.duckOthers)
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if ringTimer != nil && ringTimer.isValid {
            ringTimer.invalidate()
        }
        ringTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(playAudio), userInfo: nil, repeats: false)
    }
}
