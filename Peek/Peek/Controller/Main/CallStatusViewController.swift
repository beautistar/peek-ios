//
//  CallViewController.swift
//  Peek
//
//  Created by Yin on 2018/5/7.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class CallStatusViewController: BaseViewController {

    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var callerView: UIView!
    
    var user = UserModel()
    var status = 0
    var type = 0 // 0: call request, 1: request received and accept
    
    var alertTimer : Timer!
    
    var timerCount = 0
    
    @IBOutlet weak var callDeclineButton: UIButton!
    

    
    var targetUser = UserModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        queueVCOpened = true
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imvPhoto.layer.borderColor = UIColor.gray.cgColor
        imvPhoto.layer.cornerRadius = 50
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.masksToBounds = true
        
        imvPhoto.sd_setImage(with: URL(string: targetUser.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        lblName.text = targetUser.user_name
    }
    
    func initView() {
        
        imvPhoto.layer.borderColor = UIColor.white.cgColor
        imvPhoto.layer.borderWidth = 2.0
        imvPhoto.layer.masksToBounds = true
        
        lblName.text = user.user_name
        imvPhoto.sd_setImage(with: URL(string: user.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        
        
        if status == Const.IS_CALLING {
            
            callerView.isHidden = true
        }
        else {
            callerView.isHidden = false
        }
    }    
    
    @IBAction func callAction(_ sender: Any) {
        
        self.view.superview?.isHidden = true
//        let parentVC = self.parent as! VideoViewController
//        parentVC.connectToChatRoom()
//        parentVC.pauseAudio()
    }
    
    @IBAction func declineAction(_ sender: Any) {
        
        if self.alertTimer != nil && self.alertTimer.isValid {
            self.alertTimer.invalidate()
        }
        
        if let sinCallVC = self.parent as? SinCallViewController {
            sinCallVC.pauseAudio()
        }
        
        self.navigationController?.dismiss(animated: false, completion: nil)
        
        if status == Const.IS_CALLING {
            let sinCallVC = self.parent as! SinCallViewController
            
//            ApiRequest.rejectRequest(target_id: user.id, room_no: videoVC.roomName, completion: { (resCode) in
//
//            })
            
            if self.alertTimer != nil && self.alertTimer.isValid {
                self.alertTimer.invalidate()
            }
            //self.view.isUserInteractionEnabled = true
            
            sinCallVC.disconnect()
        }
            
        else {
            
            declineCall()
            
        }
        
    }
    
    
    func declineCall() {
        
        if self.alertTimer != nil && self.alertTimer.isValid {
            self.alertTimer.invalidate()
        }
        
        if let sinCallVC = self.parent as? SinCallViewController {
            sinCallVC.pauseAudio()
            sinCallVC.disconnect()
        }
        
        self.dismiss(animated: false, completion: nil)
        
//        if let videoVC = self.parent as? VideoViewController {
//            videoVC.pauseAudio()
//            videoVC.disconnect()
//            ApiRequest.rejectRequest(target_id: user.id, room_no: videoVC.roomName, completion: { (resCode) in
//
//            })
//        }
//
//        if isLogin {
//            self.navigationController?.dismiss(animated: false, completion: nil)
//        } else {
//            UIApplication.shared.keyWindow?.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavVC") as! UINavigationController
//        }
        
        
    }
}
