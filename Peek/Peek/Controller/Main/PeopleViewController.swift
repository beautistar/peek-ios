//
//  PeopleViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/30.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import SDWebImage

class PeopleViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var tblPeopleList: UITableView!
    
    var peopleList = [UserModel]()
    
    var callClient: SINCallClient {
        
        return _client!.call()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblPeopleList.tableFooterView = UIView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !queueVCOpened {
            getContactList()
            queueVCOpened = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getContactList() {
        
        peopleList.removeAll()
        
        self.showLoadingView()
        
        ApiRequest.getContactList(userId: (currentUser?.user_id)!) { (res_code, contactUsers) in
            
            self.hideLoadingView()
            
            if res_code == Const.CODE_SUCCESS {
                self.peopleList = contactUsers
                self.tblPeopleList.reloadData()
            } else if res_code == Const.CODE_NO_DATA {
                self.showToast(Const.NO_DATA_FOUND)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }

    
    // MARK:- tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peopleList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleCell") as! PeopleCell
        let user = peopleList[indexPath.row]
        cell.imvPhoto.sd_setImage(with: URL(string: user.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblName.text = user.user_name
        cell.btnFavorite.tag = indexPath.row
        cell.btnFavorite.addTarget(self, action: #selector(self.addFavorite(_ :)), for: .touchUpInside)
        cell.btnCall.tag = indexPath.row
        cell.btnCall.addTarget(self, action: #selector(self.callAction(_ :)), for: .touchUpInside)
        if user.invite_status == Const.IS_INVITED {
            cell.acceptButtonView.isHidden = false
            cell.acceptButton.tag = indexPath.row
            cell.acceptButton.addTarget(self, action: #selector(self.acceptInvite(_:)), for: .touchUpInside)
        } else {
            cell.acceptButtonView.isHidden = true
        }
        return cell
    }
    
    // MARK:- button Actions
    
    @objc func acceptInvite(_ sender: UIButton) {
        
        self.showLoadingView()
        
        let target_id = peopleList[sender.tag].user_id
        
        self.showLoadingView()
        ApiRequest.acceptInvite(target_id: target_id) { (result_code) in
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.showToast("You accept invitation")
                self.getContactList()
                self.tblPeopleList.reloadData()
            } else if result_code == Const.CODE_FAIL {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @objc func addFavorite(_ sender: UIButton) {
        
        let target_id = peopleList[sender.tag].user_id
        
        self.showLoadingView()
        ApiRequest.addFavorite(target_id: target_id) { (result_code) in
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.showToast("added to Favorite")
            } else if result_code == Const.CODE_205_ALREADY {
                self.showToast("Already added")
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
            self.peopleList.remove(at: sender.tag)
            self.tblPeopleList.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If a remote notification was received which led to the application being started, the may have a transition from
        // the login view controller directly to an incoming call view controller.
        if segue.identifier == "callView" {
            let callViewController = segue.destination as! CallViewController
            callViewController.call = sender as? SINCall
            callViewController.call?.delegate = callViewController
        }
    }
    
    @objc func callAction(_ sender: UIButton) {
        
        let user = self.peopleList[sender.tag]
        
        let call: SINCall = self.callClient.callUser(withId: "\(user.user_id)")
        print("call client=")
        print(self.callClient)
        print("self.destination=", "\(user.user_id)")
        self.performSegue(withIdentifier: "callView", sender: call)
        
    }
}
