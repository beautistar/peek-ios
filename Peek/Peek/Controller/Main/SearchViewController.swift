//
//  SearchViewController.swift
//  Peek
//
//  Created by Yin on 2018/5/5.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import SDWebImage

class SearchViewController: BaseViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var _from: Int = 0
    
    var searchUsers = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        searchBar.delegate = self
        self.navigationItem.title = "Search"
        tblSearch.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func sendInvite(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        let target_id = searchUsers[btn.tag].user_id
        
        self.showLoadingView()
        ApiRequest.sendInvite(target_id: target_id) { (result_code) in
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.showToast("Invitation sent")
            } else if result_code == Const.CODE_205_ALREADY {
                self.showToast("Already sent")
            }
            self.searchUsers.remove(at: btn.tag)
            self.tblSearch.reloadData()
        }
    }
    
    @objc func blockUser(_ sender: Any) {
        
        let btn = sender as! UIButton

        let target_id = searchUsers[btn.tag].user_id

        self.showLoadingView()
        ApiRequest.blockUser(target_id: target_id) { (result_code) in
            self.hideLoadingView()

            if result_code == Const.CODE_SUCCESS {
                self.showToast("success")
            } else if result_code == Const.CODE_205_ALREADY {
                self.showToast("Already blocked")
            }
            self.searchUsers.remove(at: btn.tag)
            self.tblSearch.reloadData()
        }
    }

    // MARK:- Search Result Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleCell") as! PeopleCell
        let user = searchUsers[indexPath.row]
        cell.imvPhoto.sd_setImage(with: URL(string: user.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblName.text = user.user_name
        cell.btnInvite.tag = indexPath.row
        if _from == Const.FROM_MAIN {
            cell.btnInvite.setTitle("Invite", for: .normal)
            cell.btnInvite.addTarget(self, action: #selector(self.sendInvite(_:)), for: .touchUpInside)
        } else {
            cell.btnInvite.setTitle("Block", for: .normal)
            cell.btnInvite.addTarget(self, action: #selector(self.blockUser(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    // MARK:- Search Bar
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > 0 {
            
            self.searchUsers.removeAll()
            ApiRequest.getSearchList(searchString: searchText) { (res_code, searchList) in
                
                if res_code == Const.CODE_SUCCESS {
                    self.searchUsers = searchList
                    self.tblSearch.reloadData()
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
}
