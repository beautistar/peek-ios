//
//  MainViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/29.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController, UITabBarControllerDelegate  {
    
    var btnSearch : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        setNavigationBarItem()
        initTabbar()
    }
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        self.tabBar.invalidateIntrinsicContentSize()
        
    
        let tabSize: CGFloat = 44.0
       
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = tabSize
        tabFrame.origin.y = tabSize+20
        if UIScreen.main.bounds.height == 812 { // iPhone X issue
            tabFrame.origin.y = tabSize + 44
        }
        self.tabBar.frame = tabFrame
        
        self.tabBar.isTranslucent = true
        
        initNavigationItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    func initNavigationItem() {

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17.0), NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationItem.title = Const.APP_NAME
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "9EE5FB")
        self.navigationController?.navigationBar.isTranslucent = true
               
        btnSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_search"), style:.plain,target: self, action: #selector(searchAction))
        btnSearch?.image = btnSearch?.image?.withRenderingMode(.alwaysOriginal)

        self.navigationItem.rightBarButtonItem  = btnSearch
    }
    


    func initTabbar() {
        
        self.tabBarController?.delegate = self
        
        
        if let items = self.tabBar.items
        {
            for item in items
            {
                if let image = item.image
                {
                    item.image = image.withRenderingMode( .alwaysOriginal )
                    item.selectedImage = image.withRenderingMode(.alwaysOriginal)
                }
                
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.0), NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @objc func searchAction() {
     
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC._from = Const.FROM_MAIN
        self.navigationController?.pushViewController(searchVC, animated: true)

    }
}

extension MainTabViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

