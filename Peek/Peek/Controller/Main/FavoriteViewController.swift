//
//  FavoriteViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/30.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class FavoriteViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var cvFavoriteList: UICollectionView!
    
    var favoriteList = [UserModel]()
    
    var callClient: SINCallClient {
        
        return _client!.call()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getFavoriteList()
    }

    func getFavoriteList() {
     
        self.showLoadingView()
        
        ApiRequest.getFavoriteList { (result_code, user_list) in
            
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.favoriteList = user_list
                self.cvFavoriteList.reloadData()
            } else if result_code == Const.CODE_NO_DATA {
                self.showToast(Const.NO_DATA_FOUND)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    // MARK: - Button action
    
    @objc func infoAction(_ sender: UIButton) {
        
        print("sender", sender.tag)
        
        //let user = favoriteList[sender.tag]
        
        let menuItems: [KxMenuItem] = [
        
            KxMenuItem("Mute Mic", image: #imageLiteral(resourceName: "ic_checked"), target: sender, action: #selector(pushMenuItem)),
            KxMenuItem("Close speaker", image: #imageLiteral(resourceName: "ic_unchecked"), target: sender, action: #selector(pushMenuItem)),
        ]
        
        KxMenu.show(in: sender.superview, from: sender.frame, menuItems: menuItems)
    }
    
    @objc func removeFavorite(_ sender: UIButton) {
        
        let target_id = favoriteList[sender.tag].user_id
        
        self.showLoadingView()
        ApiRequest.removeFavorite(target_id: target_id) { (result_code) in
            self.hideLoadingView()
            
            if result_code == Const.CODE_SUCCESS {
                self.showToast("Removed from Favorite")
            } else if result_code == Const.CODE_205_ALREADY {
                self.showToast("Already removed")
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
            self.favoriteList.remove(at: sender.tag)
            self.cvFavoriteList.reloadData()
        }
    }
    
    @objc func pushMenuItem(_ sender: Any) {
        
        print(sender)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If a remote notification was received which led to the application being started, the may have a transition from
        // the login view controller directly to an incoming call view controller.
        if segue.identifier == "callView" {
            let callViewController = segue.destination as! CallViewController
            callViewController.call = sender as? SINCall
            callViewController.call?.delegate = callViewController
        }
    }
   
    // MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoriteList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width / 2.0 - 5, height: UIScreen.main.bounds.width / 2.0 - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
        let user = favoriteList[indexPath.row]
        cell.imvPhoto.sd_setImage(with: URL(string: user.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
        cell.lblName.text = user.user_name
        cell.btnHeart.tag = indexPath.row
        cell.btnHeart.addTarget(self, action: #selector(removeFavorite(_:)), for: .touchUpInside)
        cell.btnInfo.tag = indexPath.row
        cell.btnInfo.addTarget(self, action: #selector(infoAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let user = favoriteList[indexPath.row]
        let call: SINCall = self.callClient.callUser(withId: "\(user.user_id)")
        
        let callVC = self.storyboard?.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
        callVC.call = call
        callVC.call?.delegate = callVC
        self.present(callVC, animated: true, completion: nil)
    }
}
