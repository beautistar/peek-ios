//
//  HistoryViewController.swift
//  Peek
//
//  Created by Yin on 2018/4/30.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblHistoryList: UITableView!
    
    var historyList = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK:- tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleCell") as! PeopleCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = UserModel()
        user.user_name = "YinFeng"
        
        //let callVC = self.storyboard?.instantiateViewController(withIdentifier: "CallingViewController") as! CallingViewController
        let sinCallVC = self.storyboard?.instantiateViewController(withIdentifier: "SinCallViewController") as! SinCallViewController
        sinCallVC.targetUser = user
        self.present(sinCallVC, animated: true, completion: nil)
    }

}
