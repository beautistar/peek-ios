//
//  CallViewController.swift
//  Peek
//
//  Created by Yin on 2018/7/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

public enum EButtonsBar: Int {
    case answerDecline = 0
    case hangup = 1
}

class CallViewController: UIViewController, SINCallDelegate {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var remoteUsername: UILabel!
    @IBOutlet weak var callStateLabel: UILabel!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var endCallButton: UIButton!
    
    @IBOutlet weak var imgEndCall: UIImageView!
    @IBOutlet weak var imgAnswerCall: UIImageView!
    @IBOutlet weak var imgDeclineCall: UIImageView!
    @IBOutlet weak var imvMic: UIImageView!
    @IBOutlet weak var imvSpeaker: UIImageView!
    @IBOutlet weak var controlView: UIView!
    
    static var sin_deferredDismissalKey: String = "sin_differredDismiss"
    //static char sin_deferredDismissalKey;
    
    var durationTimer: Timer?
    var call: SINCall?
    var isAppearing: Bool = false
    var isDisappearing: Bool = false
    
    var user = UserModel()
    var userid: Int = 0
    var isMute: Bool = false
    var isSpearkerOn: Bool = true
 
    var audioController: SINAudioController {
        
        return _client!.audioController()
    }
    
    
    func setCall(_ call: SINCall) {
        self.call = call
        self.call?.delegate = self
    }
    
    //MARK: - UIViewController Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        guard let call = self.call else { return }
        if call.direction == .incoming {
            
            callStateLabel.text = "incoming call..."
            showButtons(.answerDecline)
            audioController.startPlayingSoundFile(pathForSound("videocall.mp3"), loop: true)
        } else {
            
            callStateLabel.text = "calling..."
            showButtons(.hangup)
        }
        //controlView.isHidden = true
        checkMuteSpeaker()
        getUserInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if self.view.window == nil {
            isAppearing = false
            isDisappearing = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        isAppearing = true
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isAppearing = false
        dismissIfNecessary()
        
        guard let call = self.call else { return }
        if call.direction == .incoming {            
            showButtons(.answerDecline)
            audioController.stopPlayingSoundFile()
            call.answer()
            checkMuteSpeaker()
        } else {
            
            callStateLabel.text = "calling..."
            showButtons(.hangup)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isDisappearing = true
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isAppearing = false
    }
    // MARK: - getUserInfo
    
    func getUserInfo() {
        
        ApiRequest.getUserInfo(user_id: Int((call?.remoteUserId)!)!) { (resCode, userModel) in
            
            if resCode == Const.CODE_SUCCESS {
                self.user = userModel
                self.remoteUsername.text = self.user.user_name
                self.imgProfile.sd_setImage(with: URL(string: self.user.photo_url), placeholderImage: #imageLiteral(resourceName: "img_user"))
                
            } else {                
            }
        }
    }
    
    // MARK: - call Actions
    
    @IBAction func accept(_ sender: Any) {
        audioController.stopPlayingSoundFile()
        callStateLabel.text = "connecting..."
        call?.answer()
        checkMuteSpeaker()
    }
    
    @IBAction func decline(_ sender: Any) {
        self.call?.hangup()
        self.dismiss()
    }
    
    @IBAction func hangup(_ sender: Any) {
        self.call?.hangup()
        self.dismiss()
    }
    
    @IBAction func mute(_ sender: Any) {
        
        isMute = !isMute
        
        if isMute {
            audioController.unmute()
            imvMic.image = #imageLiteral(resourceName: "mic_on")
        } else {
            audioController.mute()
            imvMic.image = #imageLiteral(resourceName: "mic_off")
        }
    }
    
    @IBAction func speaker(_ sender: Any) {
        
        isSpearkerOn = !isSpearkerOn
        
        if isSpearkerOn {
            audioController.disableSpeaker()
            imvSpeaker.image = #imageLiteral(resourceName: "speaker_on")
        } else {
            audioController.enableSpeaker()
            imvSpeaker.image = #imageLiteral(resourceName: "speaker_off")
        }
    }
    
    //MARK: - Private functions
    
    func checkMuteSpeaker() {
        
        if isMute {
            audioController.mute()
            imvMic.image = #imageLiteral(resourceName: "mic_on")
        } else {
            audioController.unmute()
            imvMic.image = #imageLiteral(resourceName: "mic_off")
        }
        
        if isSpearkerOn {
            audioController.enableSpeaker()
            imvSpeaker.image = #imageLiteral(resourceName: "speaker_off")
        } else {
            audioController.disableSpeaker()
            imvSpeaker.image = #imageLiteral(resourceName: "speaker_on")
        }
        
    }
    
    @objc func onDurationTimer(_ unused: Timer) {
        let duration: Int = Int(Date().timeIntervalSince((self.call?.details.establishedTime)!))
        self.setDuration(duration)
    }
    
    //MARK: - SINCallDelegate
    func callDidProgress(_ call: SINCall!) {
        callStateLabel.text = "Ringing..."
        audioController.startPlayingSoundFile(self.pathForSound("ringback.wav"), loop: true)
    }
    
    func callDidEstablish(_ call: SINCall!) {
        self.startCallDurationtimerWithSelector(#selector(onDurationTimer(_:)))
        self.showButtons(.hangup)
        audioController.stopPlayingSoundFile()
        controlView.isHidden = false
    }
    
    func callDidEnd(_ call: SINCall!) {
        self.dismiss()
        audioController.stopPlayingSoundFile()
        stopCallDurationTimer()
    }
    
    func call(_ call: SINCall!, shouldSendPushNotifications pushPairs: [Any]!) {
        print("should send push")
    }
    
    //MARK: - Duration
    func setDuration(_ seconds: Int) {
        callStateLabel.text = String(format: "%02d:%02d", Int(seconds / 60), Int(seconds % 60))
    }
    
    @objc func internal_updateDuration(_ timer: Timer) {
        let selector:Selector = NSSelectorFromString(timer.userInfo as! String)
        if self.responds(to: selector) {
            self.perform(selector, with: timer)
        }
    }
    
    func startCallDurationtimerWithSelector(_ sel: Selector) {
        let selectorString = NSStringFromSelector(sel)
        self.durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(internal_updateDuration(_:)), userInfo: selectorString, repeats: true)
    }
    
    func stopCallDurationTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    // MARK: - Dismissal
    
    func dismiss() {
        
        if self.isDisappearing {
            return
        } else if self.isAppearing {
            self.setShouldDeferredDismiss(true)
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissIfNecessary() {        
      
        if self.shouldDeferrDismiss {
            if self.shouldDeferrDismiss {
                self.setShouldDeferredDismiss(false)
                DispatchQueue.main.async {
                    self.dismiss()
                }
            }
        }
    }
    
    var shouldDeferrDismiss: Bool {
        get {
            return self.sin_getAssociatedBOOLForKey(CallViewController.sin_deferredDismissalKey)
        }
    }
   
    
    func setShouldDeferredDismiss(_ v: Bool) {
        self.sin_setAssociatedBOOL(v, key: CallViewController.sin_deferredDismissalKey)
    }
    
    // MARK : -
    func sin_getAssociatedBOOLForKey(_ key: String) -> Bool {
        if let v = objc_getAssociatedObject(self, key) {
            return (v as? Bool)!
        }
        return false
    }
    
    func sin_setAssociatedBOOL(_ value: Bool, key: String) {
        objc_setAssociatedObject(self, key, value, .OBJC_ASSOCIATION_COPY)
    }
    
    

    
    // MARK
    
    private func showButtons(_ buttons: EButtonsBar) {
        if buttons == .answerDecline {
            answerButton.isHidden = false
            declineButton.isHidden = false
            endCallButton.isHidden = true
            
            imgAnswerCall.isHidden = false
            imgDeclineCall.isHidden = false
            imgEndCall.isHidden = true
            
        } else if buttons == .hangup {
            answerButton.isHidden = true
            declineButton.isHidden = true
            endCallButton.isHidden = false
            
            imgAnswerCall.isHidden = true
            imgDeclineCall.isHidden = true
            imgEndCall.isHidden = false
        }
    }
    
    //MARK: - Sounds
    
    private func pathForSound(_ name: String) -> String {
        return Bundle.main.resourcePath!.appendingFormat("/%@", name)
    }
}
